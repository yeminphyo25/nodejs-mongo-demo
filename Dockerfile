FROM node:14

RUN mkdir -p /app && chown -R node:node /app

WORKDIR /app

COPY demo/package*.json /app/

USER node

RUN npm install

COPY --chown=node:node ./demo/. /app/

EXPOSE 8080

CMD ["npm", "start"]