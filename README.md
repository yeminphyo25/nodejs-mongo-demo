**Repo paths:**
```
.
├── demo
├── manifests
│   ├── demo-nodejs.yaml
│   └── mongodb.yaml
└── Dockerfile
```

## Install the agent for Kubernetes

Before deploy the application, register an agent with the GitLab repository.<br>
To register an agent:<br>
1. On the top bar, select Menu > Projects and find your project.
2. From the left sidebar, select Infrastructure > Kubernetes clusters.
3. Select Actions.
4. From the Select an agent dropdown list, type a name for the agent.
5. Select Register an agent.

## Create an agent configuration file
To create an agent configuration file, go to the GitLab project. In the repository, create a file called config.yaml at this path:
```
.gitlab/agents/<agent-name>/config.yaml
```

## Authorize the agent to access your projects
To authorize the agent to access the GitLab project where you keep Kubernetes manifests:
1. On the top bar, select Menu > Projects and find the project that contains the agent configuration file (config.yaml).
2. Edit the file. Under the ci_access keyword, add the projects attribute.
3. For the id, add the path:
```
ci_access:
  projects:
  - id: path/to/project
```